'use strict';

var extendIE8Prototype = (function () {
/*throw new TypeError(callback + ' is not a function');
throw new TypeError(' this is null or not defined');*/
	if (!Array.prototype.indexOf) {
    	Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
	        if (this == null) {
	        	throw new TypeError(' this is null or not defined');
	        }
        	var t = Object(this),
        		len = t.length >>> 0;

	        if (len === 0) {
	            return -1;
	        }
        	var n = 0;
        	if (arguments.length > 1) {
            	n = Number(arguments[1]);
            	if (n != n) {
                	n = 0;
            	} else if (n != 0 && n != Infinity && n != -Infinity) {
                	n = (n > 0 || -1) * Math.floor(Math.abs(n));
            	}
        	}
        	if (n >= len) {
            	return -1;
        	}
        	var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        	for (; k < len; k += 1) {
            	if (k in t && t[k] === searchElement) {
                	return k;
            	}
        	}
        	return -1;
    	}
	}

	if (!String.prototype.trim) {
	    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
	    String.prototype.trim = function () {
	    	return this.replace(rtrim, '');
	    };
	}

	if (!Array.prototype.map) {
		Array.prototype.map = function (callback, thisArg) {
			var T, A, k;

		    if (this == null) {
		    	throw new TypeError(' this is null or not defined');
		    }
	    	var O = Object(this),
	    		len = O.length >>> 0;

		    if (typeof callback !== 'function') {
		      	throw new TypeError(callback + ' is not a function');
		    }

		    if (arguments.length > 1) {
		      	T = thisArg;
		    }

	    	A = new Array(len);

	    	for (k = 0; k < len; k += 1) {
	    		var kValue, mappedValue;

		      	if (k in O) {
		        	kValue = O[k];
		        	mappedValue = callback.call(T, kValue, k, O);
		        	A[k] = mappedValue;
		      	}
	    	}
	    	return A;
  		};
	}

	if (!Array.prototype.filter) {
  		Array.prototype.filter = function(callback/*, thisArg*/) {

	    	if (this === void 0 || this === null) {
	      		throw new TypeError('this is null or not defined');
	    	}

	    	var t = Object(this),
	    		len = t.length >>> 0;
	    	if (typeof callback !== 'function') {
	      		throw new TypeError(callback + ' is not a function');
	    	}

	    	var res = [],
	    		thisArg = arguments.length >= 2 ? arguments[1] : void 0;
	    
	    	for (var i = 0; i < len; i += 1) {
	      		if (i in t) {
	        		var val = t[i];
	        		if (callback.call(thisArg, val, i, t)) {
	          			res.push(val);
	        		}
	      		}
	    	}
	    	return res;
  		};
	}

	if (!Array.prototype.every) {
  		Array.prototype.every = function(callback, thisArg) {
    		var T, k;

		    if (this == null) {
		    	throw new TypeError('this is null or not defined');
		    }
    		var O = Object(this),
    			len = O.length >>> 0;

    		if (typeof callback !== 'function') {
      			throw new TypeError(callback + ' is not a function');
    		}
    		if (arguments.length > 1) {
      			T = thisArg;
    		}
    		for (k = 0; k < len; k += 1) {
    			var kValue;
      			if (k in O) {
        			kValue = O[k];
        			var testResult = callback.call(T, kValue, k, O);
        			if (!testResult) {
          				return false;
        			}
      			}
    		}
    		return true;
  		};
	}

	if (typeof Object.create != 'function') {
        var F = function () {};
        Object.create = function (o) {
            if (arguments.length > 1) { 
              throw Error('Second argument not supported');
            }
            if (o === null) { 
              throw Error('Cannot set a null [[Prototype]]');
            }
            if (typeof o != 'object') { 
              throw TypeError('Argument must be an object');
            }
            F.prototype = o;
            return new F();
        };
	}

	if (!Function.prototype.bind) {
  		Function.prototype.bind = function(oThis) {
    		if (typeof this !== 'function') {
      			throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
    		}

    		var aArgs   = Array.prototype.slice.call(arguments, 1),
		        fToBind = this,
		        fNOP    = function() {},
		        fBound  = function() {
          			return fToBind.apply(this instanceof fNOP && oThis ? this : oThis,
                 		aArgs.concat(Array.prototype.slice.call(arguments)));
        		};

			    fNOP.prototype = this.prototype;
			    fBound.prototype = new fNOP();

    			return fBound;
  		};
	}

})();

var extendBasePrototype = (function () {
	Date.prototype.getFormattedHours = function() {
	    var hours = this.getHours();
	    return hours < 10 ? '0' + hours: '' + hours;
	};

	Date.prototype.getFormattedMonth = function() {
	    var month = this.getMonth() + 1;
	    return month < 10 ? '0' + month : '' + month;
	};

	Date.prototype.getFormattedMinutes = function() {
	    var minutes = this.getMinutes();
	    return minutes < 10 ? '0' + minutes : '' + minutes;
	};
})();